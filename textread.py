filename = 'example_text.txt'
newfile = 'example_text_new.txt'
with open(filename,'r',encoding='utf-8') as f, open(newfile,'w',encoding='utf-8') as n:
    while True:
        line = f.readline()
        if line == '':
            break
        line.rstrip('\n')
        print(line)