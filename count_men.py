def count_men(filename):
    count = 0
    with open(filename,'r',encoding='utf-8') as f:
        while True:
            data = f.readline()
            if data == '':
                break
            incr_count = data.count(' men')
            incr_count += data.count(' Men')
            count +=  incr_count
    return count


if __name__ == '__main__':
    filename = 'example_text.txt'
    count = count_men(filename)
    print('Number of times counted "men" or "Men":')
    print(count)