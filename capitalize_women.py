def capitalize_women(filename, newfile):
    with open(filename, 'r', encoding='utf-8') as f, open(newfile, 'w', encoding='utf-8') as n:
        while True:
            line = f.readline()
            if line == '':
                break
            newline = line.replace('women', 'WOMEN')
            n.write(newline)


if __name__ == '__main__':
    filename = 'example_text.txt'
    newfile = 'example_text_new.txt'
    capitalize_women(filename, newfile)